package actors;

import entities.humans.MalePerson;
import entities.humans.Person;
import entities.sayings.Sayings;
import entities.servers.ServerType;

public class EmailWriter {
    private final StringBuilder message;
    private final Person recipient;
    private final Person requestedIndividuals;
    private final boolean recipientIsDefinitelyAmerican;

    public EmailWriter(Person recipient, boolean recipientIsDefinitelyAmerican, Person requestedIndividuals) {
        message = new StringBuilder();
        this.recipient = recipient;
        this.requestedIndividuals = requestedIndividuals;
        this.recipientIsDefinitelyAmerican = recipientIsDefinitelyAmerican;
        buildMessage();
    }

    public String getMessage() {
        return message.toString();
    }

    private void buildMessage() {
        message.append("Hello " + recipient.getTitle() + "Smith,");
        endLine();
        endLine();
        message.append("I hope you've been well. We will need two " + requestedIndividuals.getPluralPronoun() + " to handle deployments to the " + ServerType.SECONDARY.getNewAlias() + " servers.");
        endLine();
        message.append(Sayings.getSayingThatImpliesSimplicity(recipientIsDefinitelyAmerican));
        message.append(" So don't stress too much about who it is.");
        endLine();
        endLine();
        message.append("Best,");
        endLine();
        message.append("Dutch Van Der Linde");
    }

    private void endLine() {
        message.append("\n");
    }
}
