import actors.EmailWriter;
import entities.humans.FemalePerson;
import entities.humans.MalePerson;
import entities.humans.UnidentifiedPerson;

public class Main {
    public static void main(String[] args) {
        EmailWriter writer = new EmailWriter(new FemalePerson(), false, new UnidentifiedPerson());
        System.out.println(writer.getMessage());
    }
}
