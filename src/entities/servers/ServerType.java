package entities.servers;

public enum ServerType {
    MAIN("master", "main"),
    SECONDARY("slave", "secondary");
    String alias;
    String newAlias;
    ServerType(String alias, String newAlias) {
        this.alias = alias;
        this.newAlias = newAlias;
    }
    @Deprecated
    public String getAlias() {
        return alias;
    }
    public String getNewAlias() {
        return newAlias;
    }
}
