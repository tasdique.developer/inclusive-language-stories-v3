package entities.humans;

public class FemalePerson implements Person {
    public String getActionPronoun() {
        return "she";
    }

    public String getReferentialPronoun() {
        return "her";
    }

    public String getPossessivePronoun() {
        return "her";
    }

    public String getPluralPronoun() { return "ladies"; }

    public String getTitle() { return "Ms. "; }
}
