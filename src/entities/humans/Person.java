package entities.humans;

public interface Person {
    default String getReferentialPronoun(){
        return "them";
    }

    default String getPossessivePronoun() {
        return "their";
    }

    default String getActionPronoun() {
        return "they";
    }

    default String getPluralPronoun() { return "them"; }

    default String getTitle() { return ""; }
}
