package entities.humans;

public class NonBinaryPerson implements Person {
    private final Person identity;

    public NonBinaryPerson(Person identity) {
        this.identity = identity;
    }

    public String getActionPronoun() {
        return identity.getActionPronoun();
    }

    public String getReferentialPronoun() {
        return identity.getReferentialPronoun();
    }

    public String getPossessivePronoun() {
        return identity.getPossessivePronoun();
    }

    public String getPluralPronoun() { return identity.getPluralPronoun(); }

    public String getTitle() { return identity.getTitle(); }
}
