package entities.humans;

public class MalePerson implements Person {
    public String getActionPronoun() {
        return "he";
    }

    public String getReferentialPronoun() {
        return "him";
    }

    public String getPossessivePronoun() {
        return "his";
    }

    public String getPluralPronoun() { return "guys"; }

    public String getTitle() { return "Mr. "; }
}
