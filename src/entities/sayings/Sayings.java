package entities.sayings;

public class Sayings {
    public static String getSayingThatImpliesSimplicity(boolean isAmericanPerson) {
        return isAmericanPerson ? "It should be a piece of cake." : "It should be simple.";
    }
}
